# multiAgents.py
# --------------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
# 
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


from util import manhattanDistance
from game import Directions
import random, util

from game import Agent

#Functions to get distances
def getDistanceToClosestFood(position, foodList):
    closestDistance = 9999
    for food in foodList:
        distanceToFood = manhattanDistance(position,food)
        if distanceToFood < closestDistance: closestDistance = distanceToFood
    return closestDistance

def getDistanceToGhosts(position, ghostPosition):
    closestDistance = 9999
    for ghost in ghostPosition:
        distance= manhattanDistance(position,ghost)
        if distance < closestDistance: closestDistance = distance
    return closestDistance
    

class ReflexAgent(Agent):
    """
    A reflex agent chooses an action at each choice point by examining
    its alternatives via a state evaluation function.

    The code below is provided as a guide.  You are welcome to change
    it in any way you see fit, so long as you don't touch our method
    headers.
    """


    def getAction(self, gameState):
        """
        You do not need to change this method, but you're welcome to.

        getAction chooses among the best options according to the evaluation function.

        Just like in the previous project, getAction takes a GameState and returns
        some Directions.X for some X in the set {NORTH, SOUTH, WEST, EAST, STOP}
        """
        # Collect legal moves and successor states
        legalMoves = gameState.getLegalActions()

        # Choose one of the best actions
        scores = [self.evaluationFunction(gameState, action) for action in legalMoves]
        bestScore = max(scores)
        bestIndices = [index for index in range(len(scores)) if scores[index] == bestScore]
        chosenIndex = random.choice(bestIndices) # Pick randomly among the best

        "Add more of your code here if you want to"
        
        return legalMoves[chosenIndex]

    def evaluationFunction(self, currentGameState, action):
        """
        Design a better evaluation function here.

        The evaluation function takes in the current and proposed successor
        GameStates (pacman.py) and returns a number, where higher numbers are better.

        The code below extracts some useful information from the state, like the
        remaining food (newFood) and Pacman position after moving (newPos).
        newScaredTimes holds the number of moves that each ghost will remain
        scared because of Pacman having eaten a power pellet.

        Print out these variables to see what you're getting, then combine them
        to create a masterful evaluation function.
        """
        # Useful information you can extract from a GameState (pacman.py)
        successorGameState = currentGameState.generatePacmanSuccessor(action)
        newPos = successorGameState.getPacmanPosition()
        newFood = successorGameState.getFood()
        newGhostStates = successorGameState.getGhostStates()
        newScaredTimes = [ghostState.scaredTimer for ghostState in newGhostStates]
        
        "*** YOUR CODE HERE ***"
        # 1/distancia al menjar mes proper + distacia al fantasma
        gameScore = successorGameState.getScore()
        finalScore = gameScore
        foodScore = (1/getDistanceToClosestFood(newPos, newFood.asList()))*10
        ghostScore = getDistanceToGhosts(newPos, successorGameState.getGhostPositions())
        if ghostScore <= 2.0:
            finalScore += ghostScore*2
        else:
            finalScore += foodScore
        
        #print(f'Current Score: {currentGameState.getScore()} | Succesor score: {finalScore}')
        #print(currentGameState)
        #print(f" Final Score: {finalScore} | Food Score: {foodScore} | Ghost Score: {ghostScore} | Game Score: {gameScore}" )
        
        #finalScore = successorGameState.getScore()
        return finalScore

def scoreEvaluationFunction(currentGameState):
    """
    This default evaluation function just returns the score of the state.
    The score is the same one displayed in the Pacman GUI.

    This evaluation function is meant for use with adversarial search agents
    (not reflex agents).
    """
    return currentGameState.getScore()

class MultiAgentSearchAgent(Agent):
    """
    This class provides some common elements to all of your
    multi-agent searchers.  Any methods defined here will be available
    to the MinimaxPacmanAgent, AlphaBetaPacmanAgent & ExpectimaxPacmanAgent.

    You *do not* need to make any changes here, but you can if you want to
    add functionality to all your adversarial search agents.  Please do not
    remove anything, however.

    Note: this is an abstract class: one that should not be instantiated.  It's
    only partially specified, and designed to be extended.  Agent (game.py)
    is another abstract class.
    """

    def __init__(self, evalFn = 'scoreEvaluationFunction', depth = '2'):
        self.index = 0 # Pacman is always agent index 0
        self.evaluationFunction = util.lookup(evalFn, globals())
        self.depth = int(depth)

class MinimaxAgent(MultiAgentSearchAgent):
    """
    Your minimax agent (question 2)
    """

    def minimax(self, gameState, agent, depth):
        if gameState.isLose() or gameState.isWin() or depth == self.depth:
            return self.evaluationFunction(gameState)
        if agent == 0:  # maximax agent
            costs = []
            actions = gameState.getLegalActions(0)
            for action in actions:
                costs.append(self.minimax(gameState.generateSuccessor(agent, action), 1, depth) )
            if depth == 0: # when all the tree is expanded and you know the maximax action
                #return the action and not the cost which will not be useful
                return actions[costs.index(max(costs))]
            else: # else when scanning the tree
                return max(costs)
        else: #minimax agent
            next_agent = agent + 1  
            if gameState.getNumAgents() == next_agent:
                next_agent = 0 
                depth += 1 # when all agents actions have been explored, the cycle repeats so it's next depth level 
            costs = []
            for action in gameState.getLegalActions(agent):
                costs.append(self.minimax(gameState.generateSuccessor(agent, action), next_agent, depth) )
            return min(costs)
            

    def getAction(self, gameState):
        """
        Returns the minimax action from the current gameState using self.depth
        and self.evaluationFunction.

        Here are some method calls that might be useful when implementing minimax.

        gameState.getLegalActions(agentIndex):
        Returns a list of legal actions for an agent
        agentIndex=0 means Pacman, ghosts are >= 1

        gameState.generateSuccessor(agentIndex, action):
        Returns the successor game state after an agent takes an action

        gameState.getNumAgents():
        Returns the total number of agents in the game

        gameState.isWin():
        Returns whether or not the game state is a winning state

        gameState.isLose():
        Returns whether or not the game state is a losing state

        pacmanActions = gameState.getLegalActions(0)
        costs = {}
        for action in pacmanActions:
            costs[action] = self.minimax(gameState.generateSuccessor(0, action),0,1)
        return max(costs, key=costs.get)

        """
        "*** YOUR CODE HERE ***"
        return self.minimax(gameState, 0, 0)


class AlphaBetaAgent(MultiAgentSearchAgent):
    """
    Your minimax agent with alpha-beta pruning (question 3)
    """

    def maximizerAction(self, actions, costs, depth):
        if depth == 0: # when all the tree is expanded and you know the maximax action
            #return the action and not the cost which will not be useful
            return actions[costs.index(max(costs))]
        else: # else when scanning the tree
            return max(costs)

    def alphaBetaPrunning(self, gameState, agent, depth, alpha, beta):
        if gameState.isLose() or gameState.isWin() or depth == self.depth:
            return self.evaluationFunction(gameState)
        if agent == 0:  # maximax agent            
            actions = gameState.getLegalActions(0)
            
            costs = []
            cost = float("-inf")
            for action in actions:
                cost = max(cost, self.alphaBetaPrunning(gameState.generateSuccessor(agent, action), 1, depth, alpha, beta))
                costs.append(cost)
                if cost > beta:
                    return self.maximizerAction(actions, costs, depth)
                alpha = max(alpha,cost)                
            return self.maximizerAction(actions, costs, depth)
        
        else: #minimax agent
            next_agent = agent + 1  
            if gameState.getNumAgents() == next_agent:
                next_agent = 0 
                depth += 1 # when all agents actions have been explored, the cycle repeats so it's next depth level 
            
            costs = []
            cost = float("inf")
            for action in gameState.getLegalActions(agent):
                cost = min(cost, self.alphaBetaPrunning(gameState.generateSuccessor(agent, action), next_agent, depth, alpha, beta))
                costs.append(cost)
                if cost < alpha:
                    return min(costs)
                beta = min(beta,cost)
            return min(costs)

    def getAction(self, gameState):
        """
        Returns the minimax action using self.depth and self.evaluationFunction
        """
        "*** YOUR CODE HERE ***"
        return self.alphaBetaPrunning(gameState, 0, 0,float("-inf"),float("inf"))

class ExpectimaxAgent(MultiAgentSearchAgent):
    """
      Your expectimax agent (question 4)
    """
    def expectimax(self, depth, agent, gameState):
        if (gameState.isWin() or gameState.isLose() or depth > self.depth):
            return self.evaluationFunction(gameState)

        returnVal = []  
        actions = gameState.getLegalActions(agent)  
        if Directions.STOP in actions:
            actions.remove(Directions.STOP)

        for action in actions:
            successor = gameState.generateSuccessor(agent, action)
            if((agent+1) >= gameState.getNumAgents()):
                returnVal += [self.expectimax(depth+1, 0, successor)]
            else:
                returnVal += [self.expectimax(depth, agent+1, successor)]


        if agent == 0:
            if(depth == 1): # if back to root, return action, else ret value
                maxscore = max(returnVal)
                length = len(returnVal)
                for i in range(length):
                    if (returnVal[i] == maxscore):
                        return actions[i]
            else:
                retVal = max(returnVal)

        elif agent > 0: # ghosts
            s = sum(returnVal)
            l = len(returnVal)
            retVal = float(s/l)

        return retVal


    def getAction(self, gameState):
        """
        Returns the expectimax action using self.depth and self.evaluationFunction

        All ghosts should be modeled as choosing uniformly at random from their
        legal moves.
        """
        "*** YOUR CODE HERE ***"
        return self.expectimax(1, 0, gameState)

def isNextTo(xy1,xy2):
    if xy1[0] == xy2[0] and abs(xy1[1] - xy2[1]) == 1:
        return True
    if xy1[1] == xy2[1] and abs(xy1[0] - xy2[0]) == 1:
        return True


def betterEvaluationFunction(currentGameState):
    """
    Your extreme ghost-hunting, pellet-nabbing, food-gobbling, unstoppable
    evaluation function (question 5).

    DESCRIPTION: <write something here so we know what you did>
    """
    "*** YOUR CODE HERE ***"
    ghost_coef = 100
    # for state in currentGameState.getGhostStates():
    #     if state.newScaredTimes() != 0:
    #         ghost_coef = ghost_coef*10
    
    # scared_times = [ghostState.scaredTimer for ghostState in currentGameState.getGhostStates()]
    # print(scared_times)
    # total_time = sum(scared_times)
    # if total_time !=0:
        # print("Scared!!!")
        # ghost_coef = ghost_coef*total_time
    ghostScore = getDistanceToGhosts(currentGameState.getPacmanPosition(), currentGameState.getGhostPositions())
  
    score = 1/(len(currentGameState.getFood().asList())+1) + ghostScore/ghost_coef
    
    # for capsule in currentGameState.getCapsules():
    #     if isNextTo(capsule,currentGameState.getPacmanPosition()):
            # score = 0.001
            # print(score)
    return score

# Abbreviation
better = betterEvaluationFunction
